@echo off

SET PATH=%SystemRoot%

SET PATH=%Path%;%~dp0imagemagick
SET PATH=%Path%;%~dp0php

SET tmp=%~dp0temp
SET PHP_FCGI_MAX_REQUESTS=2000

Rem Start php-fastcgi on port 9000
REM for /l %%i in (1 1 2) do (

set PHP_HELP_MAX_REQUESTS = 100
for /l %%i in (1 1 1) do (
REM bin\runhiddenconsole php\php-cgi.exe -b 127.0.0.1:9000 -c php\php.ini
bin\runhiddenconsole php_cgi_spawner\php-cgi-spawner.exe "php\php-cgi.exe -c php\php.ini" 9000 4+16
)
exit