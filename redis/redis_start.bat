@echo off

SET PATH=%SystemRoot%

SET PATH=%Path%;%~dp0httpd\bin
SET PATH=%Path%;%~dp0imagemagick
SET PATH=%Path%;%~dp0php

SET tmp=%~dp0temp

redis-server.exe redis.conf
